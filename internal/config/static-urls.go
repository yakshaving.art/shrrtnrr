package config

import (
	"fmt"
	"io/ioutil"

	yaml "gopkg.in/yaml.v2"
)

// LoadStaticURLs loads the static urls yaml file into a map and
// returns it, or an error.
func LoadStaticURLs(filename string) (map[string]string, error) {
	m := map[string]string{}
	if filename == "" {
		return m, nil
	}

	f, err := ioutil.ReadFile(filename)
	if err != nil {
		return m, fmt.Errorf("failed to read static urls file %s: %s", filename, err)
	}

	if err := yaml.UnmarshalStrict(f, &m); err != nil {
		return m, fmt.Errorf("failed to parse static urls file %s: %s", filename, err)
	}

	return m, nil
}
